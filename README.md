# Algorithms to approximate the probabilistic bisimilarity distances for probabilistic automata
---
+ A brute force implementation of the simple policy iteration algorithm first proposed in Computation of Probabilistic Bisimilarity Distance for Probabilistic Automata by Bacci et al.

+ A brute force implementation of the value iteration algorithm proposed in Computing Game Metric on Markov Decision Processes by Fu (ICALP 2012).

+ The `PerformanceCompare.java` is for experiments to compare the simple policy iteration algorithm and the value iteration algorithm (discounted case)

+ The `PerformanceCompareUndiscounted.java` is for experiments to get the running time of the simple policy iteration algorithm for the undiscounted case

## **Getting Started**

### Google's OR-tools 

---

We use [Google's OR-tools](https://developers.google.com/optimization/) to solve the transportation problems and other optimization problems in the algorithms. 

The tool should be put under the directory `lib` and the one in this repository is for Ubuntu-16.04. If the platform to run the experiments is not Ubuntu-16.04, we need to first download the Google's OR-tools java libraries for the right platform from [or-tools java libraries](https://developers.google.com/optimization/install/java/). Then extract the folder, rename it as `or-tools` and put it under the directory `lib`.

### Run the examples
---
```shell
javac -classpath bin:lib/or-tools/lib/com.google.ortools.jar:lib/or-tools/lib/protobuf.jar src/*.java -d bin
java -Djava.library.path=lib/or-tools/lib -classpath bin:lib/or-tools/lib/com.google.ortools.jar:lib/or-tools/lib/protobuf.jar SimplePolicyIteration examples/example-input1.txt $output $discount-factor $accuracy
java -Djava.library.path=lib/or-tools/lib -classpath bin:lib/or-tools/lib/com.google.ortools.jar:lib/or-tools/lib/protobuf.jar ValueIteration examples/example-input1.txt $output $discount-factor $accuracy
```

### Input of the programme
---
The java programme takes four inputs:

+ input file:

```
	 n = number of states

	 the labels as a list of n integers

	 number of distributions of the first state 

	 each distribution on a line

	  .
	  .
	  .

	 number of distributions of the n'th state 

	 each distribution on a line
```

+ $output: the distances in the file as a `n x n` matrix.
+ $discount-factor: value in (0, 1]. (Note that for value iteration algorithm, this value should be strictly less than 1)
+ $accuracy: for example 0.00001 denotes the output distance is accurate to 5 decimal points.

### Example input files
---
The first example (Page 219 of Computing Probabilistic Bisimilarity Distances. Qiyi Tang's PhD thesis) is a probabilistic automaton which is depicted in the following figure:

![image](https://bytebucket.org/discoveri/probabilistic-bisimilarity-distances-probabilistic-automata/raw/5169d28436486601ceae8766f6a880e8664fc245/example-input.png | width=10)

The second example (Page 201 of The Complexity of Computing a Bisimilarity Pseudometric on Probabilistic Automata, Franck van Breugel and James Worrell, LNCS 8464, pp. 191-213, 2014.) is a probabilistic automaton shown as the following figure:

![image](https://bitbucket.org/discoveri/probabilistic-bisimilarity-distances-probabilistic-automata/raw/14d2fd729195c51e3398122169eaa787a9def6d6/example-input2.png)

The third example (Page 2 of Deciding Probabilistic Bisimilarity Distance One for Probabilistic Automata, Qiyi Tang and Franck van Breugel, CONCUR 18) is a probabilistic automaton shown as the following figure:

![image](https://bitbucket.org/discoveri/probabilistic-bisimilarity-distances-probabilistic-automata/raw/14d2fd729195c51e3398122169eaa787a9def6d6/example-input3.png)

- f  -  state 0 label 0

- b  -  state 1 label 0

- h  -  state 2 label 1

- t  -  state 3 label 2

### Run the experiments 

Do the following to compare the running time of simple policy iteration algorithm and the value iteration algorithm (discounted case). The results will be stored under the directory`results_compare`.
```shell
./run.sh 
```
This script will 

1. Compile the java source files
2. Run simple policy iteration algorithm, storing execution time, the number of solved transportation problems, and the number of coupling structures generated during the execution (i.e., the number of times a λ-discrepancy has been computed);
3. Then, on the same instance, it executes the value iteration algorithm until the running time exceeds that of step 2. We report the execution time, the number of solved transportation problems, and the number of iterations.
4. Finally, we report the error maxes between the distance computed in step 2 and the approximate result d obtained in step 3.

Do the following to get the running time of simple policy iteration algorithm for the undiscounted case. The results will be stored under the directory`results_undis`.
```shell
./runUndiscounted.sh 
```

## Licensing
---
This code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This extension is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You can find a copy of the GNU General Public License at http://www.gnu.org/licenses
