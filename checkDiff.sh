#!/bin/bash

for file in results_dOne/*.txt; do
  filename=$(echo $file | cut -d/ -f2)
  java CompareDistances results_dOne2/$filename results_dOne/$filename
done
