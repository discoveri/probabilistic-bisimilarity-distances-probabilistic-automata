#!/bin/bash

working_folder="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"

outputFile=$working_folder/results-undiscounted30.txt
resultsDir=$working_folder/results_undiscounted_new
#resultsDir=$working_folder/results-small/

rm -f $outputFile
for file in $resultsDir/30-*; do
  echo $file
  python processData.py $file >> $outputFile
done
