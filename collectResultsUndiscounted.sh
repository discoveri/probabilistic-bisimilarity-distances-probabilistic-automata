#!/bin/bash

working_folder="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"

outputFile=$working_folder/results.txt
resultsDir=$working_folder/results_undis
#resultsDir=$working_folder/results-small/

rm -f $outputFile
for file in $resultsDir/07*; do
  echo $file
  python processDataUndiscounted.py $file >> $outputFile
done
