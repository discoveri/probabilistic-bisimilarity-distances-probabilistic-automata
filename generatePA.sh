#!/bin/bash

working_folder="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
srcdir="$working_folder"/src
classdir="$working_folder"/bin
libdir="$working_folder"/lib/or-tools/lib/com.google.ortools.jar:"$working_folder"/lib/or-tools/lib/protobuf.jar
dynLibDir="$working_folder"/lib/or-tools/lib

# compile
javac -classpath "$classdir":"$libdir" "$srcdir"/*.java -d "$classdir"

for size in {30,40,50}; do
    for probSize in {2,3}; do
        for ndSize in {1,2,3}; do
            echo $size-$probSize-$ndSize
            java -classpath "$classdir":"$libdir" RandomGenerateProbabilisticAutomata $size $probSize $ndSize newInput/$size-$probSize-$ndSize
        done
    done
done


