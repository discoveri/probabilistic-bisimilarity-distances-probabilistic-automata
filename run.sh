#!/bin/bash

working_folder="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
srcdir="$working_folder"/src
classdir="$working_folder"/bin
libdir="$working_folder"/lib/or-tools/lib/com.google.ortools.jar:"$working_folder"/lib/or-tools/lib/protobuf.jar
dynLibDir="$working_folder"/lib/or-tools/lib

# compile
javac -classpath "$classdir":"$libdir" "$srcdir"/*.java -d "$classdir"

# run the experiments
mkdir -p $working_folder/results_discounted_new 

discount=0.8
accuracy=0.000001
fileError=errors.log
for file in $working_folder/newInput/40-3-3; do
  filename=$(echo $file | rev | cut -d / -f 1 | rev)
  echo $filename
  fileResult=$working_folder/results_discounted_new/"$filename".csv #results_undiscounted
  #-Xss20480k
  java -Djava.library.path="$dynLibDir" -classpath "$classdir":"$libdir" PerformanceCompare $file $fileResult $fileError $discount $accuracy
done





