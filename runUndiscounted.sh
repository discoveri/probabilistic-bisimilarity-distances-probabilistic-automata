#!/bin/bash

working_folder="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
srcdir="$working_folder"/src
classdir="$working_folder"/bin
libdir="$working_folder"/lib/or-tools/lib/com.google.ortools.jar:"$working_folder"/lib/or-tools/lib/protobuf.jar
dynLibDir="$working_folder"/lib/or-tools/lib

# compile
javac -classpath "$classdir":"$libdir" "$srcdir"/*.java -d "$classdir"

# run the experiments
mkdir -p $working_folder/results_coin_undis
mkdir -p $working_folder/time_coin_undis
discount=1
accuracy=0.000001
for file in $working_folder/input/coin2; do
  filename=$(echo $file | rev | cut -d / -f 1 | rev)
  echo $filename
  fileTimeResult=$working_folder/time_coin_undis/"$filename".csv
  fileDistResult=$working_folder/results_coin_undis/"$filename".csv
  java -Xss20480k -Djava.library.path="$dynLibDir" -classpath "$classdir":"$libdir" PerformanceCompareUndiscounted $file $fileTimeResult $fileDistResult input-label/coin2 $discount $accuracy
done

