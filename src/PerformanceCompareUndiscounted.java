import java.io.*;
import java.util.*;

public class PerformanceCompareUndiscounted {

    public static double getError(double[] dist1, double[] dist2) {
        double error = 0;
        for (int i = 0; i < dist1.length; i++) {
            double diff = Math.abs(dist1[i] - dist2[i]);
            if (error < diff) {
                error = diff;
            }
        }
        return error;
    }

    public static void main(String[] args) {
        Scanner input = null;
        PrintStream output = null;
        double discount = -1;
        double accuracy = 0.1;
        int numOfStates = -1;
        int[] labels = null;
        Map<Integer, Set<Distribution>> transitions = new HashMap<>();

        // parse input file
        if (args.length != 5) {
            System.out.println(
                    "Use java PerformanceCompare 0: <inputFile> 1: <outputFile> 2: <errorInputFile> 3: <discountFactor> 4: <accuracy>");
        } else {
            // process the command line arguments
            try {
                input = new Scanner(new File(args[0]));
            } catch (FileNotFoundException e) {
                System.out.printf("Input file %s not found%n", args[0]);
                System.exit(1);
            }

            try {
                output = new PrintStream(new File(args[1]));
            } catch (FileNotFoundException e) {
                System.out.printf("Output file %s not created%n", args[1]);
                System.exit(1);
            }

            int numPA = -1;
            while (input.hasNextInt()) {
                numPA++;
                try {
                    numOfStates = input.nextInt();
                    labels = new int[numOfStates];
                    for (int i = 0; i < numOfStates; i++) {
                        labels[i] = input.nextInt();
                    }

                    for (int i = 0; i < numOfStates; i++) {
                        int nDistr = input.nextInt();
                        Set<Distribution> set = new HashSet<>();
                        for (int iDistr = 0; iDistr < nDistr; iDistr++) {
                            double[] doubleArray = new double[numOfStates];
                            for (int j = 0; j < numOfStates; j++) {
                                doubleArray[j] = input.nextDouble();
                            }
                            Distribution distr = new Distribution(doubleArray);
                            set.add(distr);
                        }
                        transitions.put(i, set);
                    }

                    // input.nextLine();
                } catch (NoSuchElementException e) {
                    System.out.printf("Input file %s not in the correct format%n", args[0]);
                }

                try {
                    discount = Double.parseDouble(args[3]);
                    assert discount < 1 : String.format("Discount factor %f should be less than or equal to 1",
                            discount);
                    assert discount > 0 : String.format("Discount factor %f should be greater than 0", discount);
                } catch (NumberFormatException e) {
                    System.out.println("Discount factor not provided in the right format");
                    System.exit(1);
                }

                try {
                    accuracy = Double.parseDouble(args[4]);
                    assert accuracy <= 1 : String.format("Accuracy %f should be less than or equal to 1", accuracy);
                    assert accuracy > 0 : String.format("Accuracy %f should be greater than 0", accuracy);
                } catch (NumberFormatException e) {
                    System.out.println("Accuracy not provided in the right format");
                    System.exit(1);
                }

                // SimplePolicyIteration spi = new
                // SimplePolicyIteration(numOfStates, transitions, labels,
                // discount, accuracy);
                SimplePolicyIterationUndiscounted spi = new SimplePolicyIterationUndiscounted(numOfStates, transitions,
                        labels, accuracy);

                if (!spi.policyIteration()) {
                    continue;
                }
                // spi = new SimplePolicyIteration(numOfStates, transitions,
                // labels, discount, accuracy);
                // spi.policyIteration();
                ValueIteration vi = new ValueIteration(numOfStates, transitions, labels, discount, accuracy,
                        spi.runningTime); // Long.MAX_VALUE
                // ValueIteration vi = new ValueIteration(numOfStates,
                // transitions, labels, discount, accuracy, Long.MAX_VALUE); //

                vi.valueIteration();
                // outputTime.println(spi.runningTime);
                // let Java VM warm up
//                if (numPA < 1) {
//                    continue;
//                }
                double error = getError(spi.discrepancy, vi.discrepancy);
                output.println(spi.runningTime + ", " + spi.numTP + ", " + spi.numLP + ", " + spi.numSetM + ", "
                        + vi.runningTime + ", " + vi.numTP + ", " + vi.numIter + ", " + error);
                //System.out.println(Arrays.toString(spi.discrepancy));
                File file = new File(args[2]);
                try {
                    FileWriter fr = new FileWriter(file, true);
                    fr.write(Arrays.toString(spi.discrepancy) + "\n");
                    fr.close();
                } catch (IOException e) {
                    // exception handling left as an exercise for the reader
                }
            }
        }
    }
}

