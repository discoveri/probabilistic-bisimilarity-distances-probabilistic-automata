
//package pseudometric;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

//import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 * This application creates random probabilistic transition systems, using the
 * ErdosRenyi model.
 * 
 * @author Franck van Breugel
 */
public class RandomGenerateProbabilisticAutomata {
	private RandomGenerateProbabilisticAutomata() {
	}

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println(
					"Use java RandomGenerateProbabilisticAutomata <state size> <probabilistic choices> <nondeterministic choices> <outputFile>");
		} else {
			int stateSize = -1;
			try {
				stateSize = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.out.println("Size not provided in the right format");
				System.exit(1);
			}
			assert stateSize > 0 : "State size has to be positive";
			int probChoice = -1;
			try {
				probChoice = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				System.out.println("Number of probabilistic choices not provided in the right format");
				System.exit(1);
			}
			assert probChoice > 0 : "Number of probabilistic choices has to be positive";

			int nondeterministicChoice = -1;

			try {
				nondeterministicChoice = Integer.parseInt(args[2]);
			} catch (NumberFormatException e) {
				System.out.println("Number of nondeterministic choices not provided in the right format");
				System.exit(1);
			}
			assert nondeterministicChoice > 0 : "Number of nondeterministic choices has to be positive";

			PrintStream output = null;
			try {
				output = new PrintStream(new File(args[3]));
			} catch (FileNotFoundException e) {
				System.out.printf("Output file %s not created%n", args[1]);
				System.exit(1);
			}
			for (int numPA = 0; numPA < 100; numPA++) {
				output.println(stateSize);
				for (int source = 0; source < stateSize; source++) {
					double d = Math.random();
					if (d < 1 / 2.0) {
						output.print("0 ");
					} else {
						output.print("1 ");
					}

				}
				output.println();

				for (int source = 0; source < stateSize; source++) {
					output.println(nondeterministicChoice);
					for (int numNC = 0; numNC < nondeterministicChoice; numNC++) {
						Set<Integer> targetSet = new HashSet<>();
						while (targetSet.size() != probChoice) {
							targetSet.clear();
							for (int i = 0; i < stateSize; i++) {
								if (Math.random() < 1.0 * probChoice / stateSize) {
									targetSet.add(i);
								}
							}
						}
						for (int target = 0; target < stateSize; target++) {
							if (targetSet.contains(target)) {
								output.print(1.0 / probChoice + " ");
							} else {
								output.print(0.0 + " ");
							}
							output.print(" ");
						}
						output.println();
					}
				}
				output.println();
				// System.out.printf("Average outdegree: %.1f (%.1f)\n",
				// outdegree.getGeometricMean(),
				// outdegree.getStandardDeviation());
			}
		}
	}
}
