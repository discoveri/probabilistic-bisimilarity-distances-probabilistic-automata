import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

/**
 * * @author qiyitang
 * *
 * *
 * * Simple policy iteration algorithm first proposed in
 * * Exact Computation of Probabilistic Bisimilarity Distance for
 * Probabilistic
 * Automata
 * * by Bacci et al.
 * * $\C \leftarrow $ a random coupling
 * * while $\exists (s, t): \ell(s) = \ell(t) \land
 * \Gamma_\lambda^{\C[(s,t)/O(\gamma_\lambda^{\C},s,t)]}(\gamma_\lambda^
 * {\C})(s,t) \ls \gamma_\lambda^{\C}(s, t)$
 * * $\C = \C[(s,t)/O(\gamma_\lambda^{\C},s,t)]$
 **/
public class SimplePolicyIteration {
    public int numOfStates;
    private final Map<Integer, Set<Distribution>> transitions;
    private final int[] labels;
    private final Map<Pair, Set<Distribution>> coupling;
    public double[] discrepancy;
    private final Set<Pair> toCompute;
    public double discount = 1;
    public double accuracy;
    public long runningTime = 0;
    public long numTP = 0;
    public long numLP = 0;

    static {
        System.loadLibrary("jniortools");
    }

    public SimplePolicyIteration(int numOfStates, Map<Integer, Set<Distribution>> transitions, int[] labels,
                                 double discount, double accuracy) {
        this.numOfStates = numOfStates;
        this.transitions = transitions;
        this.labels = labels;
        this.coupling = new HashMap<Pair, Set<Distribution>>();
        this.discrepancy = new double[numOfStates * numOfStates];
        this.toCompute = new HashSet<Pair>();
        this.discount = discount;
        this.accuracy = accuracy;
    }

    /*calculate toCompute and initialize the couplings */
    public boolean initialize() {
        // toCompute
        //ProbBisimilarity probBis = new ProbBisimilarity(this.numOfStates, this.labels, this.transitions);
        Set<Pair> bisimulationSet = new HashSet<>();
        //bisimulationSet = probBis.computeProbabilisticBisimilarity();

        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < i; j++) {
                Pair p = new Pair(i, j);
                if (this.labels[i] == this.labels[j] && !bisimulationSet.contains(p)) {
                    this.toCompute.add(p);
                } else if (this.labels[i] != this.labels[j]) {
                    this.discrepancy[i * this.numOfStates + j] = 1;
                    this.discrepancy[j * this.numOfStates + i] = 1;
                }
            }
        }
        // initialize coupling
        for (Pair p : toCompute) {
            initCoupling(p);
        }
        // calculate discrepancy
        this.discrepancy = calculateDiscrepancy(this.coupling);
        return this.discrepancy != null;
    }

    /**
     * * solve the following LP
     * * objective: \min \sum (x_{s, t}) where s,t \in S
     * * constraints:
     * * x_{s, t} - discount * \sum_{u, v \in S}(\omega(u, v) * x_{u, v}) \geq 0
     * * where l(s) = l(t) and s \not~ t and \omega \in C(s, t)
     * * x_{s, t} = 1 where l(s) \not= l(t)
     * * x_{s, t} = 0 where s ~ t
     * * result: the solution of the LP is the least fixed point of \Gamma^{new
     * coupling}_{discount}
     **/
    public double[] calculateDiscrepancy(Map<Pair, Set<Distribution>> newCoupling) {
        this.numLP++;
        int size = this.numOfStates * this.numOfStates;
        MPSolver solver = new MPSolver("LinearProgramming", MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING);
        double infinity = java.lang.Double.POSITIVE_INFINITY;
        MPVariable[] array = new MPVariable[size];
        for (int i = 0; i < size; i++) {
            array[i] = solver.makeNumVar(0.0, 1.0, "x" + i);
        }

        for (int s = 0; s < this.numOfStates; s++) {
            //for (int t = 0; t < this.numOfStates; t++) {
            for (int t = 0; t < s; t++) {
                int stID = s * this.numOfStates + t;
                if (labels[s] != labels[t]) {
                    MPConstraint c = solver.makeConstraint(1, 1, "c" + stID);
                    c.setCoefficient(array[stID], 1);
                } else {
                    Pair pair = new Pair(s, t);
                    if (toCompute.contains(pair)) {
                        // case l(s) = l(t) and s not~ t
                        // debug:
                        if (!newCoupling.containsKey(pair)) {
                            System.err.println("Coupling has no key: " + pair.toString());
                            return null;
                        }
                        for (Distribution omega : newCoupling.get(pair)) {
                            MPConstraint c = solver.makeConstraint(0, 1, "c" + stID + omega.toString());

                            for (int u = 0; u < this.numOfStates; u++) {
                                for (int v = 0; v < u; v++) {
                                //for (int v = 0; v < this.numOfStates; v++) {
                                    int index = u * this.numOfStates + v;
                                    int index2= v * this.numOfStates + u;
                                    if (s == u && t == v) {
                                        c.setCoefficient(array[index], 1 - this.discount * omega.getProbability(index)-this.discount * omega.getProbability(index2));//
                                    } else {
                                        c.setCoefficient(array[index], -this.discount * omega.getProbability(index)-this.discount * omega.getProbability(index2));//
                                    }
                                }
                            }
                        }
                    } else {
                        MPConstraint c = solver.makeConstraint(0, 0, "c" + stID);
                        c.setCoefficient(array[stID], 1);
                    }
                }

            }
        }

        MPObjective objective = solver.objective();
//        for (int i = 0; i < size; i++) {
//            objective.setCoefficient(array[i], 1);
//        }
        for (int i = 0; i < this.numOfStates; i++) {
            for(int j = 0; j < this.numOfStates; j++ ) {
                int index=i*this.numOfStates+j;
                if(j < i) {
                    objective.setCoefficient(array[index], 1);
                }else{
                    objective.setCoefficient(array[index], 0);
                }
            }
        }
        objective.setMinimization();
        final MPSolver.ResultStatus resultStatus = solver.solve();
        // Check that the problem has an optimal solution.
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            /*if(solver.verifySolution(1e-3,true)){
                System.err.println("Problem with the tolerance");
            }else {*/
            System.err.println("The problem does not have an optimal solution!");
            return null;
            //}
        }

        double[] newDiscrepancy = new double[size];
        for (int i = 0; i < this.numOfStates; i++) {
            for(int j = 0; j < i; j++ ){
                int index=i*this.numOfStates+j;
                int index2=j*this.numOfStates+i;
                newDiscrepancy[index] = array[index].solutionValue();
                newDiscrepancy[index2] = array[index].solutionValue();
            }
        }
//        for (int i = 0; i < size; i++) {
//            newDiscrepancy[i] = array[i].solutionValue();
//        }
        return newDiscrepancy;
    }

    public int updateCoupling(Pair pair) {
        Map<Pair, Set<Distribution>> newCoupling = new HashMap<>(this.coupling);
        Set<Distribution> set = newCoupling.get(pair);
        Set<Distribution> newSet = new HashSet<>();
        int s = pair.getRow();
        int t = pair.getColumn();
        // newDistance =
        // \Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s, t)
        double newDistance = 0;

        int muSize = this.transitions.get(s).size();
        int nuSize = this.transitions.get(t).size();
        double[] tmpArray = new double[muSize * nuSize];
        Distribution[] distrArray = new Distribution[muSize * nuSize];
        int i = 0;
        for (Distribution mu : this.transitions.get(s)) {
            for (Distribution nu : this.transitions.get(t)) {
                SolutionPair optSolution = solveTransportationProblem(mu, nu);
                this.numTP++;
                tmpArray[i] = this.discount * optSolution.getValue();
                distrArray[i] = new Distribution(optSolution.getPoint());
                i++;
            }
        }

        // for each mu choose a nu
        for (i = 0; i < muSize; i++) {
            double minDistance = 2;
            Distribution minDistr = null;
            for (int j = 0; j < nuSize; j++) {
                int ijIndex = i * nuSize + j;
                if (tmpArray[ijIndex] < minDistance) {
                    minDistr = distrArray[ijIndex];
                    minDistance = tmpArray[ijIndex];
                }
            }
            if (minDistance > newDistance) {
                newDistance = minDistance;
            }
            newSet.add(minDistr);
        }

        // for each nu choose a mu
        for (int j = 0; j < nuSize; j++) {
            double minDistance = 2;
            Distribution minDistr = null;
            for (i = 0; i < muSize; i++) {
                int ijIndex = i * nuSize + j;
                if (tmpArray[ijIndex] < minDistance) {
                    minDistr = distrArray[ijIndex];
                    minDistance = tmpArray[ijIndex];
                }
            }
            if (minDistance > newDistance) {
                newDistance = minDistance;
            }
            newSet.add(minDistr);
        }

        // coupling at (s, t) remains the same
        if (newSet.size() == set.size() && newSet.containsAll(set)) {
            return 0;
        }

        double oldDistance = this.discrepancy[s * this.numOfStates + t];
        if (Math.abs(newDistance - oldDistance) < this.accuracy) {
            return 0;
        }
        // update the coupling if
        // (\Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s, t)
        // \ls \gamma^{old_coupling}_\lambda(s, t))
        if (newDistance < oldDistance) {
            this.coupling.replace(pair, newSet);
            this.discrepancy = calculateDiscrepancy(this.coupling);
            if (this.discrepancy == null) {
                return -1;
            }
            return 1;
        }
        return 0;
    }

    public void initCoupling(Pair pair) {
        Set<Distribution> newSet = new HashSet<>();
        int s = pair.getRow();
        int t = pair.getColumn();
        for (Distribution mu : this.transitions.get(s)) {
            for (Distribution nu : this.transitions.get(t)) {
                newSet.add(new Distribution(solveTransportationProblem(mu, nu).getPoint()));
            }
        }
        this.coupling.put(pair, newSet);
    }

    public SolutionPair solveTransportationProblem(Distribution mu, Distribution nu) {
        int size = this.numOfStates * this.numOfStates;
        MPSolver solver = new MPSolver("LinearProgramming", MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING);
        MPVariable[] array = new MPVariable[size];
        for (int i = 0; i < size; i++) {
            array[i] = solver.makeNumVar(0.0, 1, "x" + i);
        }

        for (int i = 0; i < this.numOfStates; i++) {
            MPConstraint c = solver.makeConstraint(mu.getProbability(i), mu.getProbability(i), "c" + mu.toString() + i);
            for (int j = 0; j < this.numOfStates; j++) {
                c.setCoefficient(array[i * this.numOfStates + j], 1);
            }
        }

        for (int j = 0; j < this.numOfStates; j++) {
            MPConstraint c = solver.makeConstraint(nu.getProbability(j), nu.getProbability(j), "c" + nu.toString() + j);
            for (int i = 0; i < this.numOfStates; i++) {
                c.setCoefficient(array[i * this.numOfStates + j], 1);
            }
        }

        MPObjective objective = solver.objective();

        for (int i = 0; i < size; i++) {
            objective.setCoefficient(array[i], this.discrepancy[i]);
        }

        objective.setMinimization();
        final MPSolver.ResultStatus resultStatus = solver.solve();
        // Check that the problem has an optimal solution.
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            System.err.println("The problem does not have an optimal solution!");
            return null;
        }
        double[] newDistr = new double[size];
        for (int i = 0; i < size; i++) {
            newDistr[i] = array[i].solutionValue();
        }

        SolutionPair sp = new SolutionPair(newDistr, solver.objective().value());
        return sp;
    }

    public boolean policyIteration() {
        long startTime = System.nanoTime();
        if (!initialize()) {
            return false;
        }
        // System.out.println("toCompute: " + this.toCompute.toString());
        boolean isImproved = true;
        while (isImproved) {
            isImproved = false;
            for (Pair p : toCompute) {
                int tmp = updateCoupling(p);
                if (tmp < 0) {
                    return false;
                }
                isImproved |= (tmp > 0);
            }
        }
        this.runningTime = System.nanoTime() - startTime;
        return true;
    }

    public static void main(String[] args) {
        Scanner input = null;
        PrintStream output = null;

        double discount = -1;
        double accuracy = 0.1;
        int numOfStates = -1;
        int[] labels = null;
        Map<Integer, Set<Distribution>> transitions = new HashMap<>();

        // parse input file
        if (args.length != 4) {
            System.out.println(
                    "Use java SimplePolicyIteration 0: <inputFile> 1: <outputDistanceFile> 2: <discountFactor> 3: <accuracy>");
        } else {
            // process the command line arguments
            try {
                input = new Scanner(new File(args[0]));
            } catch (FileNotFoundException e) {
                System.out.printf("Input file %s not found%n", args[0]);
                System.exit(1);
            }

            try {
                output = new PrintStream(new File(args[1]));
            } catch (FileNotFoundException e) {
                System.out.printf("Output file %s not created%n", args[1]);
                System.exit(1);
            }

            while (input.hasNextInt()) {
                try {
                    numOfStates = input.nextInt();
                    labels = new int[numOfStates];
                    for (int i = 0; i < numOfStates; i++) {
                        labels[i] = input.nextInt();
                    }

                    for (int i = 0; i < numOfStates; i++) {
                        int nDistr = input.nextInt();
                        Set<Distribution> set = new HashSet<>();
                        for (int iDistr = 0; iDistr < nDistr; iDistr++) {
                            double[] doubleArray = new double[numOfStates];
                            for (int j = 0; j < numOfStates; j++) {
                                doubleArray[j] = input.nextDouble();
                            }
                            Distribution distr = new Distribution(doubleArray);
                            set.add(distr);
                        }
                        transitions.put(i, set);
                    }

                    // input.nextLine();
                } catch (NoSuchElementException e) {
                    System.out.printf("Input file %s not in the correct format%n", args[0]);
                }

                try {
                    discount = Double.parseDouble(args[2]);
                    assert discount < 1 : String.format("Discount factor %f should be less than or equal to 1",
                            discount);
                    assert discount > 0 : String.format("Discount factor %f should be greater than 0", discount);
                } catch (NumberFormatException e) {
                    System.out.println("Discount factor not provided in the right format");
                    System.exit(1);
                }

                try {
                    accuracy = Double.parseDouble(args[3]);
                    assert accuracy <= 1 : String.format("Accuracy %f should be less than or equal to 1", accuracy);
                    assert accuracy > 0 : String.format("Accuracy %f should be greater than 0", accuracy);
                } catch (NumberFormatException e) {
                    System.out.println("Accuracy not provided in the right format");
                    System.exit(1);
                }

                SimplePolicyIteration spi = null;
                spi = new SimplePolicyIteration(numOfStates, transitions, labels, discount, accuracy);
                if (!spi.policyIteration()) {
                    continue;
                }

                String format = "%." + -(int) Math.log10(spi.accuracy) + "f ";
                // output the distances
                for (int i = 0; i < spi.numOfStates; i++) {
                    for (int j = 0; j < spi.numOfStates; j++) {
                        output.printf(format, spi.discrepancy[i * spi.numOfStates + j]);
                    }
                    output.println();
                }
                output.println();

            }
        }
    }
}

class SolutionPair {
    double[] transportPlan;
    double opt;

    public SolutionPair(double[] array, double value) {
        this.transportPlan = array;
        this.opt = value;
    }

    public double getValue() {
        return opt;
    }

    public double[] getPoint() {
        return transportPlan;
    }
}
