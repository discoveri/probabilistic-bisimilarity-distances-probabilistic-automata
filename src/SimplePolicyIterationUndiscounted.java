import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class SimplePolicyIterationUndiscounted {
    public static int numOfStates;
    private Map<Integer, Set<Distribution>> transitions;
    private int[] labels;
    private Map<Pair, Set<Distribution>> coupling;
    public double[] discrepancy;
    private Set<Pair> toCompute;
    public double accuracy;
    public long runningTime = 0;
    public long numTP = 0;
    public long numLP = 0;
    public long numSetM = 0;
    public Set<Pair> distanceOneSet = new HashSet<>();

    static {
        System.loadLibrary("jniortools");
    }

    public SimplePolicyIterationUndiscounted(int numOfStates, Map<Integer, Set<Distribution>> transitions, int[] labels,
                                             double accuracy) {
        this.numOfStates = numOfStates;
        this.transitions = transitions;
        this.labels = labels;
        this.coupling = new HashMap<Pair, Set<Distribution>>();
        this.discrepancy = new double[numOfStates * numOfStates];
        this.toCompute = new HashSet<Pair>();
        this.accuracy = accuracy;
    }

    /*calculate toCompute and initialize the couplings */
    public boolean initialize() {
        // toCompute
        Set<Pair> bisimulationSet = new HashSet<>();

        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < i; j++) {
                Pair p = new Pair(i, j);
                if (this.labels[i] == this.labels[j] && !bisimulationSet.contains(p)) {
                    this.toCompute.add(p);
                } else if (this.labels[i] != this.labels[j]) {
                    this.discrepancy[i * this.numOfStates + j] = 1;
                    this.discrepancy[j * this.numOfStates + i] = 1;
                }
            }
        }

        // initialize coupling
        for (Pair p : toCompute) {
            initCoupling(p);
        }
        // calculate discrepancy
        this.discrepancy = calculateDiscrepancy(this.coupling);
        if (this.discrepancy == null) {
            return false;
        }
        return true;
    }

    /**
     * * solve the following LP
     * * objective: \min \sum (x_{s, t}) where s,t \in S
     * * constraints:
     * * x_{s, t} - discount * \sum_{u, v \in S}(\omega(u, v) * x_{u, v}) \geq 0
     * * where l(s) = l(t) and s \not~ t and \omega \in C(s, t)
     * * x_{s, t} = 1 where l(s) \not= l(t)
     * * x_{s, t} = 0 where s ~ t
     * * result: the solution of the LP is the least fixed point of \Gamma^{new
     * coupling}_{discount}
     **/
    public double[] calculateDiscrepancy(Map<Pair, Set<Distribution>> newCoupling) {
        this.numLP++;
        int size = this.numOfStates * this.numOfStates;
        MPSolver solver = new MPSolver("LinearProgramming", MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING);
        double infinity = java.lang.Double.POSITIVE_INFINITY;
        MPVariable[] array = new MPVariable[size];
        for (int i = 0; i < size; i++) {
            array[i] = solver.makeNumVar(0.0, 1.0, "x" + i);
        }

        for (int s = 0; s < this.numOfStates; s++) {
            //for (int t = 0; t < this.numOfStates; t++) {
            for (int t = 0; t < s; t++) {
                int stID = s * this.numOfStates + t;
                if (labels[s] != labels[t]) {
                    MPConstraint c = solver.makeConstraint(1, 1, "c" + stID);
                    c.setCoefficient(array[stID], 1);
                } else {
                    Pair pair = new Pair(s, t);
                    if (toCompute.contains(pair)) {
                        // case l(s) = l(t) and s not~ t
                        // debug:
                        if (!newCoupling.containsKey(pair)) {
                            System.err.println("Coupling has no key: " + pair.toString());
                            return null;
                        }
                        for (Distribution omega : newCoupling.get(pair)) {
                            MPConstraint c = solver.makeConstraint(0, 1, "c" + stID + omega.toString());

                            for (int u = 0; u < this.numOfStates; u++) {
                                for (int v = 0; v < u; v++) {
                                    //for (int v = 0; v < this.numOfStates; v++) {
                                    int index = u * this.numOfStates + v;
                                    int index2 = v * this.numOfStates + u;
                                    if (s == u && t == v) {
                                        c.setCoefficient(array[index], 1 - omega.getProbability(index) - omega.getProbability(index2));//
                                    } else {
                                        c.setCoefficient(array[index], -omega.getProbability(index) - omega.getProbability(index2));//
                                    }
                                }
                            }
                        }
                    } else {
                        MPConstraint c = solver.makeConstraint(0, 0, "c" + stID);
                        c.setCoefficient(array[stID], 1);
                    }
                }

            }
        }

        MPObjective objective = solver.objective();
//        for (int i = 0; i < size; i++) {
//            objective.setCoefficient(array[i], 1);
//        }
        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < this.numOfStates; j++) {
                int index = i * this.numOfStates + j;
                if (j < i) {
                    objective.setCoefficient(array[index], 1);
                } else {
                    objective.setCoefficient(array[index], 0);
                }
            }
        }
        objective.setMinimization();
        final MPSolver.ResultStatus resultStatus = solver.solve();
        // Check that the problem has an optimal solution.
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            /*if(solver.verifySolution(1e-3,true)){
                System.err.println("Problem with the tolerance");
            }else {*/
            System.err.println("The problem does not have an optimal solution!");
            return null;
            //}
        }
        double[] newDiscrepancy = new double[size];
        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < i; j++) {
                int index = i * this.numOfStates + j;
                int index2 = j * this.numOfStates + i;
                newDiscrepancy[index] = array[index].solutionValue();
                newDiscrepancy[index2] = array[index].solutionValue();
            }
        }
//        for (int i = 0; i < size; i++) {
//            newDiscrepancy[i] = array[i].solutionValue();
//        }
        return newDiscrepancy;
    }

    public int updateCoupling(Pair pair) {
        Map<Pair, Set<Distribution>> newCoupling = new HashMap<>(this.coupling);
        Set<Distribution> set = newCoupling.get(pair);
        Set<Distribution> newSet = new HashSet<>();
        int s = pair.getRow();
        int t = pair.getColumn();
        // newDistance =
        // \Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s, t)
        double newDistance = 0;

        int muSize = this.transitions.get(s).size();
        int nuSize = this.transitions.get(t).size();
        double[] tmpArray = new double[muSize * nuSize];
        Distribution[] distrArray = new Distribution[muSize * nuSize];
        int i = 0;
        for (Distribution mu : this.transitions.get(s)) {
            for (Distribution nu : this.transitions.get(t)) {
                SolutionPair optSolution = solveTransportationProblem(mu, nu, this.discrepancy);
                this.numTP++;
                tmpArray[i] = optSolution.getValue();
                distrArray[i] = new Distribution(optSolution.getPoint());
                i++;
            }
        }

        // for each mu choose a nu
        for (i = 0; i < muSize; i++) {
            double minDistance = 2;
            Distribution minDistr = null;
            for (int j = 0; j < nuSize; j++) {
                int ijIndex = i * nuSize + j;
                if (tmpArray[ijIndex] < minDistance) {
                    minDistr = distrArray[ijIndex];
                    minDistance = tmpArray[ijIndex];
                }
            }
            if (minDistance > newDistance) {
                newDistance = minDistance;
            }
            newSet.add(minDistr);
        }

        // for each nu choose a mu
        for (int j = 0; j < nuSize; j++) {
            double minDistance = 2;
            Distribution minDistr = null;
            for (i = 0; i < muSize; i++) {
                int ijIndex = i * nuSize + j;
                if (tmpArray[ijIndex] < minDistance) {
                    minDistr = distrArray[ijIndex];
                    minDistance = tmpArray[ijIndex];
                }
            }
            if (minDistance > newDistance) {
                newDistance = minDistance;
            }
            newSet.add(minDistr);
        }

        // coupling at (s, t) remains the same
        if (newSet.size() == set.size() && newSet.containsAll(set)) {
            return 0;
        }

        double oldDistance = this.discrepancy[s * this.numOfStates + t];
        if (Math.abs(newDistance - oldDistance) < this.accuracy) {
            return 0;
        }
        // update the coupling if
        // (\Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s, t)
        // \ls \gamma^{old_coupling}_\lambda(s, t))
        if (newDistance < oldDistance) {
            this.coupling.replace(pair, newSet);
            this.discrepancy = calculateDiscrepancy(this.coupling);
            if (this.discrepancy == null) {
                return -1;
            }
            return 1;
        }
        return 0;
    }

    public void initCoupling(Pair pair) {
        Set<Distribution> newSet = new HashSet<>();
        int s = pair.getRow();
        int t = pair.getColumn();
        for (Distribution mu : this.transitions.get(s)) {
            for (Distribution nu : this.transitions.get(t)) {
                //newSet.add(new Distribution(solveTransportationProblem(mu, nu, this.discrepancy).getPoint()));
                newSet.add(initialSolution(mu, nu));
            }
        }
        this.coupling.put(pair, newSet);
    }

    // TP initial solution North-West Corner method
    private Distribution initialSolution(Distribution mu, Distribution nu) {
        double[] arr = new double[this.numOfStates * this.numOfStates];

        class Support {
            int state;
            double prob;

            Support(int s, double p) {
                this.state = s;
                this.prob = p;
            }
        }
        Support[] srcTmp = new Support[this.numOfStates];
        for (int i = 0; i < this.numOfStates; i++) {
            srcTmp[i] = new Support(i, mu.getProbability(i));
        }
        Support[] tgtTmp = new Support[this.numOfStates];
        for (int i = 0; i < this.numOfStates; i++) {
            tgtTmp[i] = new Support(i, nu.getProbability(i));
        }

        class SupportComparator implements Comparator<Support> {

            @Override
            public int compare(Support o1, Support o2) {
                // TODO Auto-generated method stub
                if (o1.prob == o2.prob)
                    return 0;
                else if (o1.prob > o2.prob)
                    return -1;
                else
                    return 1;
            }

        }

        Arrays.sort(srcTmp, new SupportComparator());
        Arrays.sort(tgtTmp, new SupportComparator());

        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < this.numOfStates; j++) {
                double min = Math.min(srcTmp[i].prob, tgtTmp[j].prob);
                //System.out.println("prob: " + srcTmp[i].prob + " " + tgtTmp[j].prob + " ");
                int u = srcTmp[i].state;
                int v = tgtTmp[j].state;
                //System.out.println("state: " + u + " " + v + " ");

                arr[u * this.numOfStates + v] = min;
                //arr[v*this.numOfStates + u] = min;
                srcTmp[i].prob = srcTmp[i].prob - min;
                tgtTmp[j].prob = tgtTmp[j].prob - min;
            }
        } // end of for
        return new Distribution(arr);
    }

    public static SolutionPair solveTransportationProblem(Distribution mu, Distribution nu, double[] distance) {
        int size = numOfStates * numOfStates;
        MPSolver solver = new MPSolver("LinearProgramming", MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING);
        MPVariable[] array = solver.makeNumVarArray(size, 0.0, 1.0);

        for (int i = 0; i < numOfStates; i++) {
            MPConstraint c = solver.makeConstraint(mu.getProbability(i), mu.getProbability(i));
            for (int j = 0; j < numOfStates; j++) {
                c.setCoefficient(array[i * numOfStates + j], 1);
            }
        }

        for (int j = 0; j < numOfStates; j++) {
            MPConstraint c = solver.makeConstraint(nu.getProbability(j), nu.getProbability(j));
            for (int i = 0; i < numOfStates; i++) {
                c.setCoefficient(array[i * numOfStates + j], 1);
            }
        }

        MPObjective objective = solver.objective();

        for (int i = 0; i < size; i++) {
            objective.setCoefficient(array[i], distance[i]);
        }

        objective.setMinimization();
        final MPSolver.ResultStatus resultStatus = solver.solve();
        // Check that the problem has an optimal solution.
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            System.err.println("The TP problem does not have an optimal solution!");
            return null;
        }
        double[] newDistr = new double[size];
        for (int i = 0; i < size; i++) {
            newDistr[i] = array[i].solutionValue();
        }

        SolutionPair sp = new SolutionPair(newDistr, solver.objective().value());
        return sp;
    }

    private boolean checkSupportSet(double[] omega, Set<Pair> set) {
        for (int i = 0; i < this.numOfStates; i++) {
            for (int j = 0; j < this.numOfStates; j++) {
                if (omega[i * this.numOfStates + j] > 0 && !set.contains(new Pair(i, j))) {
                    return false;
                }
            }
        }
        return true;
    }

    public Set<Pair> calculateSelfClosedSet() {

        Set<Pair> setM = new HashSet<>(this.toCompute);

        // exclude the pairs that have distance 0
        Iterator<Pair> iter = setM.iterator();
        while (iter.hasNext()) {
            Pair p = iter.next();
            int stIndex = p.getRow() * this.numOfStates + p.getColumn();
            if (this.discrepancy[stIndex] == 0) {
                iter.remove();
            }
        }

        if (setM.isEmpty()) {
            return setM;
        }

        // iterate until reach the greatest fixed point
        boolean isNotFixedPoint = false;
        while (!isNotFixedPoint) {
            isNotFixedPoint = true;
            Set<Pair> newSet = new HashSet<>(setM);
            Iterator<Pair> iterNew = newSet.iterator();
            while (iterNew.hasNext()) {
                Pair p = iterNew.next();
                int s = p.getRow();
                int t = p.getColumn();
                int stIndex = s * this.numOfStates + t;
                // check mu
                boolean isRemoved = false;

                for (Distribution mu : this.transitions.get(s)) {
                    for (Distribution nu : this.transitions.get(t)) {
                        SolutionPair sp = this.solveTransportationProblem(mu, nu, this.discrepancy);
                        double sol = sp.getValue();
                        if (Math.abs(sol - this.discrepancy[stIndex]) <= this.accuracy) {
                            double[] omega = sp.getPoint();
                            if (!checkSupportSet(omega, setM)) {
                                iterNew.remove();
                                isNotFixedPoint = false;
                                isRemoved = true;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        break;
                    }
                }

                if (isRemoved) {
                    continue;
                }

                // check nu
                for (Distribution nu : this.transitions.get(t)) {
                    for (Distribution mu : this.transitions.get(s)) {
                        SolutionPair sp = this.solveTransportationProblem(mu, nu, this.discrepancy);
                        double sol = sp.getValue();
                        if (Math.abs(sol - this.discrepancy[stIndex]) <= this.accuracy) {
                            double[] omega = sp.getPoint();
                            if (!checkSupportSet(omega, setM)) {
                                iterNew.remove();
                                isNotFixedPoint = false;
                                isRemoved = true;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        break;
                    }
                }
            }
            setM = newSet;
        }
        return setM;
    }

    public double calculateTheta(Set<Pair> setM) {
        double theta = 1;
        for (Pair p : setM) {
            // System.out.println(p);
            double thetap = getMinTheta(p);
            // System.out.println(dist);

            theta = (thetap < theta) ? thetap : theta;
            // System.out.println(theta);

        }
        return theta;
    }

    private double getMinTheta(Pair pair) {
        int s = pair.getRow();
        int t = pair.getColumn();
        int muSize = this.transitions.get(s).size();
        int nuSize = this.transitions.get(t).size();
        double pointDistance = this.discrepancy[s * this.numOfStates + t];

        double[] tmpArray = new double[muSize * nuSize];

        int i = 0;
        for (Distribution mu : this.transitions.get(s)) {
            for (Distribution nu : this.transitions.get(t)) {
                tmpArray[i] = this.solveTransportationProblem(mu, nu, this.discrepancy).getValue();
                i++;
            }
        }

        double thetaST = pointDistance;
        // get theta_s
        for (i = 0; i < muSize; i++) {
            double minNu = 1;
            for (int j = 0; j < nuSize; j++) {
                double tmp = tmpArray[i * nuSize + j];
                minNu = (tmp < minNu) ? tmp : minNu;
            }
            double tmp = pointDistance - minNu;
            thetaST = (tmp > 0 && tmp < thetaST) ? tmp : thetaST;
        }

        // get theta_t
        for (int j = 0; j < nuSize; j++) {
            double minMu = 1;
            for (i = 0; i < muSize; i++) {
                double tmp = tmpArray[i * nuSize + j];
                minMu = (tmp < minMu) ? tmp : minMu;
            }
            double tmp = pointDistance - minMu;
            thetaST = (tmp > 0 && tmp < thetaST) ? tmp : thetaST;
        }
        return thetaST;
    }

    public void validateDiscrepancy(Set<Pair> pairSet) {
        double[] distArray = Arrays.copyOf(this.discrepancy, this.numOfStates * this.numOfStates);
        // for (Pair pair : this.toCompute) {
        for (int s = 0; s < this.numOfStates; s++) {
            for (int t = 0; t < s; t++) {
                // newDistance =
                // \Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s,
                // t)
                Pair p = new Pair(s, t);
                if (this.toCompute.contains(p)) {
                    double newDistance = 0;

                    int muSize = this.transitions.get(s).size();
                    int nuSize = this.transitions.get(t).size();
                    double[] tmpArray = new double[muSize * nuSize];
                    int i = 0;
                    for (Distribution mu : this.transitions.get(s)) {
                        for (Distribution nu : this.transitions.get(t)) {
                            SolutionPair optSolution = solveTransportationProblem(mu, nu, this.discrepancy);
                            this.numTP++;
                            tmpArray[i] = optSolution.getValue();
                            i++;
                        }
                    }

                    // for each mu choose a nu
                    for (i = 0; i < muSize; i++) {
                        double minDistance = 2;
                        for (int j = 0; j < nuSize; j++) {
                            int ijIndex = i * nuSize + j;
                            if (tmpArray[ijIndex] < minDistance) {
                                minDistance = tmpArray[ijIndex];
                            }
                        }
                        if (minDistance > newDistance) {
                            newDistance = minDistance;
                        }
                    }

                    // for each nu choose a mu
                    for (int j = 0; j < nuSize; j++) {
                        double minDistance = 2;
                        for (i = 0; i < muSize; i++) {
                            int ijIndex = i * nuSize + j;
                            if (tmpArray[ijIndex] < minDistance) {
                                minDistance = tmpArray[ijIndex];
                            }
                        }
                        if (minDistance > newDistance) {
                            newDistance = minDistance;
                        }
                    }
                    distArray[s * this.numOfStates + t] = newDistance;
                    distArray[t * this.numOfStates + s] = newDistance;
                } else {

                }

            }
        }
        this.discrepancy = distArray;
    }

    public void adjustDiscrepancy(double theta, Set<Pair> setM) {
        for (Pair pair : setM) {
            int s = pair.getRow();
            int t = pair.getColumn();
            int stIndex = s * this.numOfStates + t;

            double tmp = this.discrepancy[s * this.numOfStates + t];
            this.discrepancy[stIndex] = tmp - theta;
            this.discrepancy[t * this.numOfStates + s] = this.discrepancy[stIndex];
        }
    }

    public void updateAllCouplings() {

        for (Pair pair : this.toCompute) {
            int s = pair.getRow();
            int t = pair.getColumn();

            Set<Distribution> newSet = new HashSet<>();
            double newDistance = 0;

            int muSize = this.transitions.get(s).size();
            int nuSize = this.transitions.get(t).size();
            double[] tmpArray = new double[muSize * nuSize];
            Distribution[] distrArray = new Distribution[muSize * nuSize];
            int i = 0;
            for (Distribution mu : this.transitions.get(s)) {
                for (Distribution nu : this.transitions.get(t)) {
                    SolutionPair optSolution = solveTransportationProblem(mu, nu, this.discrepancy);
                    this.numTP++;
                    tmpArray[i] = optSolution.getValue();
                    distrArray[i] = new Distribution(optSolution.getPoint());
                    i++;
                }
            }

            // for each mu choose a nu
            for (i = 0; i < muSize; i++) {
                double minDistance = 2;
                Distribution minDistr = null;
                for (int j = 0; j < nuSize; j++) {
                    int ijIndex = i * nuSize + j;
                    if (tmpArray[ijIndex] < minDistance) {
                        minDistr = distrArray[ijIndex];
                        minDistance = tmpArray[ijIndex];
                    }
                }
                if (minDistance > newDistance) {
                    newDistance = minDistance;
                }
                newSet.add(minDistr);
            }

            // for each nu choose a mu
            for (int j = 0; j < nuSize; j++) {
                double minDistance = 2;
                Distribution minDistr = null;
                for (i = 0; i < muSize; i++) {
                    int ijIndex = i * nuSize + j;
                    if (tmpArray[ijIndex] < minDistance) {
                        minDistr = distrArray[ijIndex];
                        minDistance = tmpArray[ijIndex];
                    }
                }
                if (minDistance > newDistance) {
                    newDistance = minDistance;
                }
                newSet.add(minDistr);
            }

            // update the coupling if
            // (\Gamma^{new_coupling}_\lambda(\gamma^{old_coupling}_\lambda)(s,
            // t)
            // \ls \gamma^{old_coupling}_\lambda(s, t))
            this.coupling.replace(pair, newSet);
        }
        this.discrepancy = calculateDiscrepancy(this.coupling);
    }

    public boolean policyIteration() {
        long startTime = System.nanoTime();
        if (!initialize()) {
            return false;
        }
        // System.out.println("mu size 1: " + this.transitions.get(1).size());
        // System.out.println("mu size 2: " + this.transitions.get(2).size());

        boolean isMin = false;
        while (!isMin) {
            isMin = true;
            //System.out.println("toCompute: " + this.toCompute.toString());
            boolean isImproved = true;
            while (isImproved) {
                isImproved = false;
                for (Pair p : toCompute) {
                    int tmp = updateCoupling(p);
                    if (tmp < 0) {
                        return false;
                    }
                    isImproved |= (tmp > 0);
                }
            }

            // validateDiscrepancy(this.toCompute);
            //System.out.println("After: " + Arrays.toString(this.discrepancy));

            Set<Pair> setM = this.calculateSelfClosedSet();
            // update discrepancy
            //System.out.println("setM" + setM);
            if (!setM.isEmpty()) {
                numSetM++;
                isMin = false;
                double theta = this.calculateTheta(setM);
                //System.out.println("theta = " + theta);
                //System.out.println("Before: " + Arrays.toString(this.discrepancy));
                adjustDiscrepancy(theta, setM);
                //System.out.println("After: " + Arrays.toString(this.discrepancy));

                updateAllCouplings();
            }
        }
        this.runningTime = System.nanoTime() - startTime;
        return true;
    }

    public static void main(String[] args) {
        Scanner input = null;
        PrintStream output = null;

        double accuracy = 0.1;
        int numOfStates = -1;
        int[] labels = null;
        Map<Integer, Set<Distribution>> transitions = new HashMap<>();

        // parse input file
        if (args.length != 3) {
            System.out.println("Use java SimplePolicyIteration 0: <inputFile> 1: <outputDistanceFile> 2: <accuracy>");
        } else {
            // process the command line arguments
            try {
                input = new Scanner(new File(args[0]));
            } catch (FileNotFoundException e) {
                System.out.printf("Input file %s not found%n", args[0]);
                System.exit(1);
            }

            try {
                output = new PrintStream(new File(args[1]));
            } catch (FileNotFoundException e) {
                System.out.printf("Output file %s not created%n", args[1]);
                System.exit(1);
            }

            while (input.hasNextInt()) {
                try {
                    numOfStates = input.nextInt();
                    labels = new int[numOfStates];
                    for (int i = 0; i < numOfStates; i++) {
                        labels[i] = input.nextInt();
                    }

                    for (int i = 0; i < numOfStates; i++) {
                        int nDistr = input.nextInt();
                        Set<Distribution> set = new HashSet<>();
                        for (int iDistr = 0; iDistr < nDistr; iDistr++) {
                            double[] doubleArray = new double[numOfStates];
                            for (int j = 0; j < numOfStates; j++) {
                                doubleArray[j] = input.nextDouble();
                            }
                            Distribution distr = new Distribution(doubleArray);
                            set.add(distr);
                        }
                        transitions.put(i, set);
                    }

                    // input.nextLine();
                } catch (NoSuchElementException e) {
                    System.out.printf("Input file %s not in the correct format%n", args[0]);
                }

                try {
                    accuracy = Double.parseDouble(args[2]);
                    assert accuracy <= 1 : String.format("Accuracy %f should be less than or equal to 1", accuracy);
                    assert accuracy > 0 : String.format("Accuracy %f should be greater than 0", accuracy);
                } catch (NumberFormatException e) {
                    System.out.println("Accuracy not provided in the right format");
                    System.exit(1);
                }

                SimplePolicyIterationUndiscounted spi = null;
                spi = new SimplePolicyIterationUndiscounted(numOfStates, transitions, labels, accuracy);
                if (!spi.policyIteration()) {
                    continue;
                }

                String format = "%." + -(int) Math.log10(spi.accuracy) + "f ";
                // output the distances
                for (int i = 0; i < spi.numOfStates; i++) {
                    for (int j = 0; j < spi.numOfStates; j++) {
                        System.out.printf(format, spi.discrepancy[i * spi.numOfStates + j]);
                    }
                    System.out.println();
                }
                System.out.println();

            }
        }
    }
}

