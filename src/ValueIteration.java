import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/*
 * The value iteration algorithm to approximate
 * the DISCOUNTED probabilistic bisimilarity distances (proposed by Deng et al. (QAPL’05))
 * for probabilistic automata.
 * This algorithm is implied in Section~4 of
 * Computing Game Metric on Markov Decision Processes by Fu (ICALP 2012)
 * */

public class ValueIteration {
    public int numOfStates;
    private Map<Integer, Set<Distribution>> transitions;
    private int[] labels;
    public double[] discrepancy;
    public double discount = 1;
    public double accuracy;
    public long runningTime = 0;
    private long timeLimit = 0;
    public long numTP = 0;
    public long numIter = 0;

    static {
        System.loadLibrary("jniortools");
    }

    public ValueIteration(int numOfStates, Map<Integer, Set<Distribution>> transitions, int[] labels, double discount,
                          double accuracy, long timeLimit) {
        this.numOfStates = numOfStates;
        this.transitions = transitions;
        this.labels = labels;
        this.discrepancy = new double[numOfStates * numOfStates];
        this.discount = discount;// discount \in (0, 1)
        this.accuracy = accuracy;
        this.timeLimit = timeLimit;
    }

    public double[] applyDelta() {
        double[] newDiscrepancy = new double[numOfStates * numOfStates];
        for (int s = 0; s < this.numOfStates; s++) {
            for (int t = 0; t < s; t++) {
                int stIndex = s * this.numOfStates + t;
                int tsIndex = t * this.numOfStates + s;

                if (this.labels[s] != this.labels[t]) {
                    newDiscrepancy[stIndex] = 1;
                    newDiscrepancy[tsIndex] = 1;
                } else {
                    int muSize = this.transitions.get(s).size();
                    int nuSize = this.transitions.get(t).size();
                    double[] tmpArray = new double[muSize * nuSize];
                    int i = 0;
                    for (Distribution mu : this.transitions.get(s)) {
                        for (Distribution nu : this.transitions.get(t)) {
                            tmpArray[i] = this.solveTransportationProblem(mu, nu);
                            i++;
                        }
                    }

                    // \max_{\mu \in \delta(s)}\min_{\nu \in
                    // \delta(t)}K(d)(\mu,\nu)
                    double maxMu = 0;
                    for (i = 0; i < muSize; i++) {
                        double minNu = 2;
                        for (int j = 0; j < nuSize; j++) {
                            int ijIndex = i * nuSize + j;
                            if (tmpArray[ijIndex] < minNu) {
                                minNu = tmpArray[ijIndex];
                            }
                        }
                        if (minNu > maxMu) {
                            maxMu = minNu;
                        }
                    }
                    // \max_{\nu \in \delta(t)}\min_{\mu \in
                    // \delta(s)}K(d)(\mu,\nu)
                    for (int j = 0; j < nuSize; j++) {
                        double minMu = 2;
                        for (i = 0; i < muSize; i++) {
                            int ijIndex = i * nuSize + j;
                            if (tmpArray[ijIndex] < minMu) {
                                minMu = tmpArray[ijIndex];
                            }
                        }
                        if (minMu > maxMu) {
                            maxMu = minMu;
                        }
                    }

                    newDiscrepancy[stIndex] = this.discount * maxMu;
                    newDiscrepancy[tsIndex] = newDiscrepancy[stIndex];
                }
            }
        }
        return newDiscrepancy;
    }

    public double solveTransportationProblem(Distribution mu, Distribution nu) {
        this.numTP++;
        int size = this.numOfStates * this.numOfStates;
        MPSolver solver = new MPSolver(
                "LinearProgramming", MPSolver.OptimizationProblemType.CLP_LINEAR_PROGRAMMING);
        MPVariable[] array = new MPVariable[size];
        for (int i = 0; i < size; i++) {
            array[i] = solver.makeNumVar(0.0, 1, "x" + i);
        }

        for (int i = 0; i < this.numOfStates; i++) {
            MPConstraint c = solver.makeConstraint(mu.getProbability(i), mu.getProbability(i), "c" + mu.toString() + i);
            for (int j = 0; j < this.numOfStates; j++) {
                c.setCoefficient(array[i * this.numOfStates + j], 1);
            }
        }

        for (int j = 0; j < this.numOfStates; j++) {
            MPConstraint c = solver.makeConstraint(nu.getProbability(j), nu.getProbability(j), "c" + nu.toString() + j);
            for (int i = 0; i < this.numOfStates; i++) {
                c.setCoefficient(array[i * this.numOfStates + j], 1);
            }
        }

        MPObjective objective = solver.objective();
        //objective.setCoefficient(x, 3);
        //objective.setCoefficient(y, 4);
        for (int i = 0; i < size; i++) {
            objective.setCoefficient(array[i], this.discrepancy[i]);
        }

        objective.setMinimization();
        final MPSolver.ResultStatus resultStatus = solver.solve();
        // Check that the problem has an optimal solution.
        if (resultStatus != MPSolver.ResultStatus.OPTIMAL) {
            System.err.println("The problem does not have an optimal solution!");
            return -1;
        }

        return solver.objective().value();
    }

    public int compareDiscrepancy(double[] disc1, double[] disc2) {
        boolean isGreater = false;
        boolean isLess = false;
        for (int i = 0; i < disc1.length; i++) {
            if (Math.abs(disc1[i] - disc2[i]) < this.accuracy) {
                continue;
            }
            if (disc1[i] < disc2[i]) {
                isLess = true;
            }
            if (disc1[i] > disc2[i]) {
                isGreater = true;
            }
        }
        if (isGreater && isLess) {
            return -321; // warning big problem!
        }
        if (isGreater) {
            return 1;
        }
        if (isLess) {
            return -1;
        }
        return 0;
    }

    public void valueIteration() {
        long startTime = System.nanoTime();
        long endTime = 0;
        boolean isImproved = true;
        while (isImproved) {
            endTime = System.nanoTime();
            if (endTime - startTime > this.timeLimit) {
                this.runningTime = endTime - startTime;
                return;
            }
            numIter++;
            isImproved = false;
            double[] newDiscrepancy = applyDelta();
            if (compareDiscrepancy(this.discrepancy, newDiscrepancy) != 0) {
                this.discrepancy = newDiscrepancy;
                isImproved = true;
            } else {
                this.discrepancy = newDiscrepancy;
            }
        }
        this.runningTime = System.nanoTime() - startTime;
    }

    public static void main(String[] args) {
        Scanner input = null;
        PrintStream output = null;
        double discount = -1;
        double accuracy = 0.1;
        int numOfStates = -1;
        int[] labels = null;
        Map<Integer, Set<Distribution>> transitions = new HashMap<>();

        // parse input file
        if (args.length != 4) {
            System.out.println(
                    "Use java ValueIteration 0: <inputFile> 1: <outputDistanceFile> 2: <discountFactor> 3: <accuracy>");
        } else {
            // process the command line arguments
            try {
                input = new Scanner(new File(args[0]));
            } catch (FileNotFoundException e) {
                System.out.printf("Input file %s not found%n", args[0]);
                System.exit(1);
            }

            try {
                output = new PrintStream(new File(args[1]));
            } catch (FileNotFoundException e) {
                System.out.printf("Output file %s not created%n", args[1]);
                System.exit(1);
            }

            while (input.hasNextInt()) {
                try {
                    numOfStates = input.nextInt();
                    labels = new int[numOfStates];
                    for (int i = 0; i < numOfStates; i++) {
                        labels[i] = input.nextInt();
                    }

                    // for(int i = 0; i < numOfTrans; i++ ){
                    // int row = input.nextInt();
                    // int col = input.nextInt();
                    // tranFunc[row][col] = input.nextDouble();
                    // }
                    for (int i = 0; i < numOfStates; i++) {
                        int nDistr = input.nextInt();
                        Set<Distribution> set = new HashSet<>();
                        for (int iDistr = 0; iDistr < nDistr; iDistr++) {
                            double[] doubleArray = new double[numOfStates];
                            for (int j = 0; j < numOfStates; j++) {
                                doubleArray[j] = input.nextDouble();
                            }
                            Distribution distr = new Distribution(doubleArray);
                            set.add(distr);
                        }
                        transitions.put(i, set);
                    }

                    // input.nextLine();
                } catch (NoSuchElementException e) {
                    System.out.printf("Input file %s not in the correct format%n", args[0]);
                }

                try {
                    discount = Double.parseDouble(args[2]);
                    assert discount < 1 : String.format("Discount factor %f should be less than 1", discount);
                    assert discount > 0 : String.format("Discount factor %f should be greater than 0", discount);
                } catch (NumberFormatException e) {
                    System.out.println("Discount factor not provided in the right format");
                    System.exit(1);
                }
                try {
                    accuracy = Double.parseDouble(args[3]);
                    assert accuracy <= 1 : String.format("Accuracy %f should be less than or equal to 1", accuracy);
                    assert accuracy > 0 : String.format("Accuracy %f should be greater than 0", accuracy);
                } catch (NumberFormatException e) {
                    System.out.println("Accuracy not provided in the right format");
                    System.exit(1);
                }
                ValueIteration vi = null;
                vi = new ValueIteration(numOfStates, transitions, labels, discount, accuracy, Integer.MAX_VALUE);
                vi.valueIteration();


                String format = "%." + -(int) Math.log10(vi.accuracy) + "f ";
                // output the distances
                for (int i = 0; i < vi.numOfStates; i++) {
                    for (int j = 0; j < vi.numOfStates; j++) {
                        output.printf(format, vi.discrepancy[i * vi.numOfStates + j]);
                    }
                    output.println();
                }
                output.println();
            }
        }

    }

}
